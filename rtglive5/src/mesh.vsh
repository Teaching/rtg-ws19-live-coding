
uniform mat4 uView;
uniform mat4 uProj;

in vec3 aPosition;
in vec3 aNormal;
in vec3 aColor;

out vec3 vColor;
out vec3 vNormal;

void main()
{
    vColor = aColor;
    vNormal = aNormal;
    gl_Position = uProj * uView * vec4(aPosition, 1);
}
