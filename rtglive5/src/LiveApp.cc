#include "LiveApp.hh"

#include <imgui/imgui.h>

#include <GLFW/glfw3.h>

#include <typed-geometry/tg-std.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow/data/TextureData.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureRectangle.hh>

#include <glow-extras/geometry/Quad.hh>

void LiveApp::init()
{
    setGui(Gui::ImGui);

    GlfwApp::init();

    auto src_folder = glow::util::pathOf(__FILE__);

    mShaderMesh = glow::Program::createFromFile(src_folder + "/mesh");
    mShaderTerrain = glow::Program::createFromFile(src_folder + "/terrain");
    mShaderOutput = glow::Program::createFromFile(src_folder + "/output");

    mSkyBox = glow::TextureCubeMap::createFromData(glow::TextureData::createFromFileCube( //
        src_folder + "/../../textures/posx.jpg",                                                         //
        src_folder + "/../../textures/negx.jpg",                                                         //
        src_folder + "/../../textures/posy.jpg",                                                         //
        src_folder + "/../../textures/negy.jpg",                                                         //
        src_folder + "/../../textures/posz.jpg",                                                         //
        src_folder + "/../../textures/negz.jpg",                                                         //
        glow::ColorSpace::sRGB));

    mQuad = glow::geometry::Quad<>().generate();

    mTargetColor = glow::TextureRectangle::create(1, 1, GL_RGB8);
    mTargetDepth = glow::TextureRectangle::create(1, 1, GL_DEPTH_COMPONENT32);
    mFramebuffer = glow::Framebuffer::create("fColor", mTargetColor, mTargetDepth);

    // init terrain
    {
        tg::rng rng;
        mHeightMap.resize(mTerrainSize * mTerrainSize);
        for (auto z = 0; z < mTerrainSize; ++z)
            for (auto x = 0; x < mTerrainSize; ++x)
                mHeightMap[idxOf(x, z)] = uniform(rng, -.8f, .8f);

        mHeightMapTexture = glow::Texture2D::create();
        {
            auto tex = mHeightMapTexture->bind();
            tex.setData(GL_R32F, mTerrainSize, mTerrainSize, mHeightMap);
            tex.setWrap(GL_REPEAT, GL_REPEAT);
            tex.generateMipmaps();
        }

        if (false) // old terrain
            for (auto z = 0; z < mTerrainSize - 1; ++z)
                for (auto x = 0; x < mTerrainSize - 1; ++x)
                {
                    auto make_pos = [&](int x, int z) {
                        auto h = mHeightMap[idxOf(x, z)];
                        return tg::pos3(x - mTerrainSize / 2, h, z - mTerrainSize / 2);
                    };

                    auto p00 = make_pos(x + 0, z + 0);
                    auto p01 = make_pos(x + 0, z + 1);
                    auto p10 = make_pos(x + 1, z + 0);
                    auto p11 = make_pos(x + 1, z + 1);

                    debugMesh.add_triangle(p00, p11, p10, tg::color3::green);
                    debugMesh.add_triangle(p00, p01, p11, tg::color3::green);
                }
    }

    debugMesh.add_triangle({1, 1, 0}, {7, 1, -1}, {4, 1, -3}, tg::color3::red);
    debugMesh.add_triangle({3, 0, -2}, {1, 0, -3}, {-2, 0, 5}, tg::color3::blue);
    debugMesh.upload();
}

void LiveApp::update(float elapsedSeconds)
{
    // TODO

    auto const speed = 50.f;

    if (mKeyDownW)
        cam.pos += cam.dir * elapsedSeconds * speed;
    if (mKeyDownS)
        cam.pos -= cam.dir * elapsedSeconds * speed;
    if (mKeyDownD)
        cam.pos += cam.right() * elapsedSeconds * speed;
    if (mKeyDownA)
        cam.pos -= cam.right() * elapsedSeconds * speed;
}

void LiveApp::render(float elapsedSeconds)
{
    auto const w = getWindowWidth();
    auto const h = getWindowHeight();

    cam.aspect_ratio = w / float(h);

    // camera matrices
    auto proj = cam.proj();
    auto view = cam.view();
    auto inv_proj = inverse(proj);
    auto inv_view = inverse(view);

    // render scene
    {
        auto fb = mFramebuffer->bind();

        // clear screen
        GLOW_SCOPED(clearColor, tg::color3::black);
        GLOW_SCOPED(enable, GL_DEPTH_TEST);
        GLOW_SCOPED(enable, GL_CULL_FACE);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        GLOW_SCOPED(polygonMode, mWireframe ? GL_LINE : GL_FILL);

        {
            auto shader = mShaderMesh->use();
            shader["uProj"] = proj;
            shader["uView"] = view;

            debugMesh.vertexArray->bind().draw();
        }

        {
            auto shader = mShaderTerrain->use();
            shader["uProj"] = proj;
            shader["uView"] = view;
            shader["uTerrainSize"] = mTerrainSize;
            shader["uViewDistance"] = mViewDistance;
            shader["uHeightMap"] = mHeightMapTexture;
            shader["uCenterPos"] = tg::round(cam.pos);

            mQuad->bind().draw(mViewDistance * mViewDistance);
        }
    }

    // read back screen pixel
    {
        auto fb = mFramebuffer->bind();

        // GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, void *pixels

        auto mx = int(getMousePosition().x);
        auto my = int(getWindowHeight() - getMousePosition().y - 1);

        if (0 <= mx && 0 <= my && mx < getWindowWidth() && my < getWindowHeight())
        {
            glReadPixels(mx, my, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &mDepthBelowCursor);

            tg::pos3 ndc = {mx / (getWindowWidth() - 1.f) * 2 - 1,  //
                            my / (getWindowHeight() - 1.f) * 2 - 1, //
                            mDepthBelowCursor * 2 - 1};

            mWorldPosBelowCursor = inv_view * inv_proj * ndc;
        }
    }

    // write output
    {
        auto shader = mShaderOutput->use();
        shader["uTargetColor"] = mTargetColor;
        shader["uTargetDepth"] = mTargetDepth;
        shader["uSkyBox"] = mSkyBox;
        shader["uInvProj"] = inv_proj;
        shader["uInvView"] = inv_view;

        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);

        mQuad->bind().draw();
    }
}

void LiveApp::onResize(int w, int h)
{
    GlfwApp::onResize(w, h);

    mTargetColor->bind().resize(w, h);
    mTargetDepth->bind().resize(w, h);
}

void LiveApp::onGui()
{
    if (ImGui::Begin("debug"))
    {
        ImGui::Checkbox("Wireframe", &mWireframe);

        auto mp = getMousePosition();
        ImGui::Text("mouse x: %f", mp.x);
        ImGui::Text("mouse y: %f", mp.y);
        ImGui::Text("depth: %f", mDepthBelowCursor);
        ImGui::Text("world x: %f", mWorldPosBelowCursor.x);
        ImGui::Text("world y: %f", mWorldPosBelowCursor.y);
        ImGui::Text("world z: %f", mWorldPosBelowCursor.z);
        ImGui::SliderInt("view distance", &mViewDistance, 10, 1000);
    }
    ImGui::End();
}

bool LiveApp::onMouseButton(double x, double y, int button, int action, int mods, int clickCount)
{
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
        setCursorMode(glow::glfw::CursorMode::Disabled);

    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
        setCursorMode(glow::glfw::CursorMode::Normal);

    return true;
}

bool LiveApp::onMousePosition(double x, double y)
{
    static double lastX = x;
    static double lastY = y;

    auto dx = float(x - lastX);
    auto dy = float(y - lastY);

    auto cam_speed = -360_deg / 2000;

    if (isMouseButtonDown(1))
    {
        auto rotX = tg::rotation_around(cam_speed * dx, cam.up);
        cam.dir = normalize(rotX * cam.dir);
        auto rotY = tg::rotation_around(cam_speed * dy, cam.right());
        cam.dir = normalize(rotY * cam.dir);
    }

    lastX = x;
    lastY = y;

    return true;
}

bool LiveApp::onKey(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_A && action == GLFW_PRESS)
        mKeyDownA = true;
    if (key == GLFW_KEY_A && action == GLFW_RELEASE)
        mKeyDownA = false;

    if (key == GLFW_KEY_W && action == GLFW_PRESS)
        mKeyDownW = true;
    if (key == GLFW_KEY_W && action == GLFW_RELEASE)
        mKeyDownW = false;

    if (key == GLFW_KEY_D && action == GLFW_PRESS)
        mKeyDownD = true;
    if (key == GLFW_KEY_D && action == GLFW_RELEASE)
        mKeyDownD = false;

    if (key == GLFW_KEY_S && action == GLFW_PRESS)
        mKeyDownS = true;
    if (key == GLFW_KEY_S && action == GLFW_RELEASE)
        mKeyDownS = false;

    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
        mKeyDownSpace = true;
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE)
        mKeyDownSpace = false;

    return false;
}

void mesh::upload()
{
    arrayBuffer = glow::ArrayBuffer::create();
    arrayBuffer->defineAttribute(&vertex::pos, "aPosition");
    arrayBuffer->defineAttribute(&vertex::normal, "aNormal");
    arrayBuffer->defineAttribute(&vertex::color, "aColor");
    arrayBuffer->bind().setData(vertices);

    vertexArray = glow::VertexArray::create(arrayBuffer, GL_TRIANGLES);
}
