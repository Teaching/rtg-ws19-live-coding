uniform sampler2DRect uTargetColor;
uniform sampler2DRect uTargetDepth;

uniform samplerCube uSkyBox;

uniform mat4 uInvProj;
uniform mat4 uInvView;

in vec2 vPosition;

out vec3 fColor;

void main()
{
    vec4 near = vec4(vPosition * 2 - 1, -1, 1);
    vec4 far = vec4(vPosition * 2 - 1, 1, 1);
    near = uInvProj * near;
    far = uInvProj * far;
    near = uInvView * near;
    far = uInvView * far;
    near /= near.w;
    far /= far.w;

    vec3 V = (far - near).xyz;

    float d = texture(uTargetDepth, gl_FragCoord.xy).x;

    if (d == 1)
        fColor = texture(uSkyBox, V).rgb;
    else
        fColor = texture(uTargetColor, gl_FragCoord.xy).rgb;

}
