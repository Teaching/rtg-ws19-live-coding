uniform sampler2D uTexAlbedo;
uniform sampler2D uTexNormal;
uniform sampler2D uTexAO;
uniform sampler2D uTexHeight;
uniform sampler2D uTexRoughness;

uniform float uEpsMultiplier;
uniform float uRuntime;

in vec3 vNormal;
in vec3 vColor;
in vec3 vWorldPos;

out vec3 fColor;

void main()
{
    vec2 uv = vWorldPos.xz / 5;

    float apparentHeight = 0.1;

    vec3 N = vec3(0,1,0);
    vec3 T = vec3(1,0,0);
    vec3 B = vec3(0,0,1);

    vec3 lightPos = vec3(4 * cos(uRuntime), 3 + 2 * cos(uRuntime * 0.3), 4 * sin(uRuntime));
    vec3 L = normalize(lightPos - vWorldPos);

    vec3 albedo = texture(uTexAlbedo, uv).rgb;
    vec3 normal = texture(uTexNormal, uv).rgb;
    float ao = texture(uTexAO, uv).x;
    float h = texture(uTexHeight, uv).x;

    float eps = 1. / textureSize(uTexHeight, 0).x * uEpsMultiplier;

    float hpx = texture(uTexHeight, uv + vec2(eps, 0)).x;
    float hnx = texture(uTexHeight, uv - vec2(eps, 0)).x;
    float hpy = texture(uTexHeight, uv + vec2(0, eps)).x;
    float hny = texture(uTexHeight, uv - vec2(0, eps)).x;

    float dh_dx = (hpx - hnx) / (2 * eps) * apparentHeight;
    float dh_dy = (hpy - hny) / (2 * eps) * apparentHeight;

    N = normalize(N - dh_dx * T - dh_dy * B);

    fColor = albedo * ao * max(0, dot(N, L));
    // fColor = albedo;

    // fColor = vColor * max(0, normalize(vNormal).y);
}
