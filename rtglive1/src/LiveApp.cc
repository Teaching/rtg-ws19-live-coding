#include "LiveApp.hh"

#include <imgui/imgui.h>

#include <GLFW/glfw3.h>

#include <typed-geometry/tg.hh>

#include <glow-extras/vector/backend/opengl.hh>
#include <glow-extras/vector/graphics2D.hh>
#include <glow-extras/vector/image2D.hh>
#include <glow/common/scoped_gl.hh>

void LiveApp::init()
{
    setGui(Gui::ImGui);

    // centering the player
    mPlayer.pos.x = 500;
    mPlayer.pos.y = 500;
    mPlayer.dir = tg::dir2::pos_x;

    tg::rng rng;
    auto const bb = tg::aabb2(-10000, 10000);
    for (auto i = 0; i < 20; ++i)
    {
        Asteroid a;
        a.pos = uniform(rng, bb);
        a.velocity = tg::uniform<tg::dir2>(rng) * uniform(rng, 0.0f, 10.0f);
        mAsteroids.push_back(a);
    }

    GlfwApp::init();
}

void LiveApp::update(float elapsedSeconds)
{
    auto const w = getWindowWidth();
    auto const h = getWindowHeight();

    // update player
    {
        if (mKeyDownW)
            mPlayer.pos += mPlayer.dir * elapsedSeconds * 300;
        if (mKeyDownS)
            mPlayer.pos -= mPlayer.dir * elapsedSeconds * 300;
        if (mKeyDownA)
        {
            auto rot_angle = -360_deg * elapsedSeconds;
            auto [sa, ca] = tg::sin_cos(rot_angle);
            auto A = tg::mat2::from_rows({ca, -sa}, {sa, ca});
            mPlayer.dir = normalize(A * mPlayer.dir);
        }
        if (mKeyDownD)
        {
            auto rot_angle = 360_deg * elapsedSeconds;
            auto [sa, ca] = tg::sin_cos(rot_angle);
            auto A = tg::mat2::from_rows({ca, -sa}, {sa, ca});
            mPlayer.dir = normalize(A * mPlayer.dir);
        }

        while (mPlayer.pos.x > w)
            mPlayer.pos.x -= w;
        while (mPlayer.pos.y > h)
            mPlayer.pos.y -= h;
        while (mPlayer.pos.x < 0)
            mPlayer.pos.x += w;
        while (mPlayer.pos.y < 0)
            mPlayer.pos.y += h;
    }

    // update asteroids
    for (auto& a : mAsteroids)
    {
        a.pos += a.velocity * elapsedSeconds;

        while (a.pos.x > w)
            a.pos.x -= w;
        while (a.pos.y > h)
            a.pos.y -= h;
        while (a.pos.x < 0)
            a.pos.x += w;
        while (a.pos.y < 0)
            a.pos.y += h;
    }
}

void LiveApp::render(float elapsedSeconds)
{
    auto const w = getWindowWidth();
    auto const h = getWindowHeight();

    // clear screen
    GLOW_SCOPED(clearColor, tg::color3::black);
    glViewport(0, 0, w, h);
    glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    mImage.clear();
    auto g = graphics(mImage);

    // render scene
    for (auto ox : {-w, 0, w})
        for (auto oy : {-h, 0, h})
        {
            auto offset = tg::vec2(ox, oy);

            // render player
            auto n = tg::dir2(-mPlayer.dir.y, mPlayer.dir.x);
            auto p0 = mPlayer.pos + offset + mPlayer.dir * 40;
            auto p1 = mPlayer.pos + offset - mPlayer.dir * 10 + n * 10;
            auto p2 = mPlayer.pos + offset - mPlayer.dir * 10 - n * 10;
            g.draw(tg::segment2(p0, p1), {tg::color3::white, 3});
            g.draw(tg::segment2(p1, p2), {tg::color3::white, 3});
            g.draw(tg::segment2(p2, p0), {tg::color3::white, 3});

            // render asteroids
            for (auto const& a : mAsteroids)
            {
                g.draw(tg::circle2(a.pos + offset, a.size), {tg::color3::white, 3});
            }
        }
    // TODO


    // render vector image
    mRenderer.render(mImage, w, h);
}

void LiveApp::onGui()
{
    // TODO
}

bool LiveApp::onKey(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_A && action == GLFW_PRESS)
        mKeyDownA = true;
    if (key == GLFW_KEY_A && action == GLFW_RELEASE)
        mKeyDownA = false;

    if (key == GLFW_KEY_W && action == GLFW_PRESS)
        mKeyDownW = true;
    if (key == GLFW_KEY_W && action == GLFW_RELEASE)
        mKeyDownW = false;

    if (key == GLFW_KEY_D && action == GLFW_PRESS)
        mKeyDownD = true;
    if (key == GLFW_KEY_D && action == GLFW_RELEASE)
        mKeyDownD = false;

    if (key == GLFW_KEY_S && action == GLFW_PRESS)
        mKeyDownS = true;
    if (key == GLFW_KEY_S && action == GLFW_RELEASE)
        mKeyDownS = false;

    // TODO
    return false;
}
