#pragma once

#include <glow-extras/glfw/GlfwApp.hh>

#include <glow-extras/vector/backend/opengl.hh>
#include <glow-extras/vector/image2D.hh>

#include <typed-geometry/tg-lean.hh>

struct Player
{
    tg::pos2 pos;
    tg::dir2 dir;
    tg::vec2 velocity;
};

struct Asteroid
{
    tg::pos2 pos;
    tg::vec2 velocity;
    float size = 30;
    float health = 10;
};

class LiveApp : public glow::glfw::GlfwApp
{
    glow::vector::OGLRenderer mRenderer;
    glow::vector::image2D mImage;

    Player mPlayer;
    std::vector<Asteroid> mAsteroids;

    bool mKeyDownW = false;
    bool mKeyDownA = false;
    bool mKeyDownS = false;
    bool mKeyDownD = false;

public:
    void init() override;

    void update(float elapsedSeconds) override;
    void render(float elapsedSeconds) override;

    void onGui() override;

    bool onKey(int key, int scancode, int action, int mods) override;
};
