#include "LiveApp.hh"

#include <imgui/imgui.h>

#include <GLFW/glfw3.h>

#include <typed-geometry/tg-std.hh>

#include <glow/common/scoped_gl.hh>

void LiveApp::init()
{
    setGui(Gui::ImGui);

    GlfwApp::init();

    mMeshShader = glow::Program::createFromFile("mesh");

    // init terrain
    {
        tg::rng rng;
        mHeightMap.resize(mMapSize * mMapSize);
        for (auto z = 0; z < mMapSize; ++z)
            for (auto x = 0; x < mMapSize; ++x)
                mHeightMap[idxOf(x, z)] = uniform(rng, -.8f, .8f);

        for (auto z = 0; z < mMapSize - 1; ++z)
            for (auto x = 0; x < mMapSize - 1; ++x)
            {
                auto make_pos = [&](int x, int z) {
                    auto h = mHeightMap[idxOf(x, z)];
                    return tg::pos3(x - mMapSize / 2, h, z - mMapSize / 2);
                };

                auto p00 = make_pos(x + 0, z + 0);
                auto p01 = make_pos(x + 0, z + 1);
                auto p10 = make_pos(x + 1, z + 0);
                auto p11 = make_pos(x + 1, z + 1);

                debugMesh.add_triangle(p00, p11, p10, tg::color3::green);
                debugMesh.add_triangle(p00, p01, p11, tg::color3::green);
            }
    }

    debugMesh.add_triangle({1, 1, 0}, {7, 1, -1}, {4, 1, -3}, tg::color3::red);
    debugMesh.add_triangle({3, 0, -2}, {1, 0, -3}, {-2, 0, 5}, tg::color3::blue);
    debugMesh.upload();
}

void LiveApp::update(float elapsedSeconds)
{
    // TODO

    auto const speed = 10.f;

    if (mKeyDownW)
        cam.pos += cam.dir * elapsedSeconds * speed;
    if (mKeyDownS)
        cam.pos -= cam.dir * elapsedSeconds * speed;
    if (mKeyDownD)
        cam.pos += cam.right() * elapsedSeconds * speed;
    if (mKeyDownA)
        cam.pos -= cam.right() * elapsedSeconds * speed;
}

void LiveApp::render(float elapsedSeconds)
{
    auto const w = getWindowWidth();
    auto const h = getWindowHeight();

    cam.aspect_ratio = w / float(h);

    // clear screen
    GLOW_SCOPED(clearColor, tg::color3::black);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    GLOW_SCOPED(enable, GL_CULL_FACE);
    glViewport(0, 0, w, h);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // camera matrices
    auto proj = cam.proj();
    auto view = cam.view();

    {
        auto shader = mMeshShader->use();
        shader["uProj"] = proj;
        shader["uView"] = view;

        debugMesh.vertexArray->bind().draw();
    }
}

void LiveApp::onGui()
{
    // TODO
}

bool LiveApp::onMousePosition(double x, double y)
{
    static double lastX = x;
    static double lastY = y;

    auto dx = float(x - lastX);
    auto dy = float(y - lastY);

    auto cam_speed = -360_deg / 2000;

    auto rotX = tg::rotation_around(cam_speed * dx, cam.up);
    cam.dir = normalize(rotX * cam.dir);
    auto rotY = tg::rotation_around(cam_speed * dy, cam.right());
    cam.dir = normalize(rotY * cam.dir);

    lastX = x;
    lastY = y;

    return true;
}

bool LiveApp::onKey(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_A && action == GLFW_PRESS)
        mKeyDownA = true;
    if (key == GLFW_KEY_A && action == GLFW_RELEASE)
        mKeyDownA = false;

    if (key == GLFW_KEY_W && action == GLFW_PRESS)
        mKeyDownW = true;
    if (key == GLFW_KEY_W && action == GLFW_RELEASE)
        mKeyDownW = false;

    if (key == GLFW_KEY_D && action == GLFW_PRESS)
        mKeyDownD = true;
    if (key == GLFW_KEY_D && action == GLFW_RELEASE)
        mKeyDownD = false;

    if (key == GLFW_KEY_S && action == GLFW_PRESS)
        mKeyDownS = true;
    if (key == GLFW_KEY_S && action == GLFW_RELEASE)
        mKeyDownS = false;

    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
        mKeyDownSpace = true;
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE)
        mKeyDownSpace = false;

    return false;
}

void mesh::upload()
{
    arrayBuffer = glow::ArrayBuffer::create();
    arrayBuffer->defineAttribute(&vertex::pos, "aPosition");
    arrayBuffer->defineAttribute(&vertex::normal, "aNormal");
    arrayBuffer->defineAttribute(&vertex::color, "aColor");
    arrayBuffer->bind().setData(vertices);

    vertexArray = glow::VertexArray::create(arrayBuffer, GL_TRIANGLES);
}
