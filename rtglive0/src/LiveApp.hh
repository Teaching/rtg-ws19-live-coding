#pragma once

#include <glow-extras/glfw/GlfwApp.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/objects/Shader.hh>

#include <typed-geometry/tg-lean.hh>

struct particle
{
    tg::pos2 pos;
    tg::vec2 velocity;
    tg::color3 color;
    float lifetime = 0;
};

class LiveApp : public glow::glfw::GlfwApp
{
    glow::SharedArrayBuffer mVertexData;
    glow::SharedVertexArray mVertexArray;
    glow::SharedShader mVertexShader;
    glow::SharedShader mFragmentShader;
    glow::SharedProgram mProgram;

    std::vector<particle> mVertices;

public:
    void init() override;

    void render(float elapsedSeconds) override;

    void onGui() override;
};
