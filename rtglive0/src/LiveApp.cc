#include "LiveApp.hh"

#include <imgui/imgui.h>

#include <typed-geometry/tg.hh>

#include <glow/common/scoped_gl.hh>

tg::rng rng;

void LiveApp::init()
{
    setGui(Gui::ImGui);

    GlfwApp::init();

    mVertexData = glow::ArrayBuffer::create();
    mVertexData->defineAttribute(&particle::pos, "aPosition");
    mVertexData->defineAttribute(&particle::color, "aColor");
    mVertexArray = glow::VertexArray::create(mVertexData, GL_POINTS);

    mVertexShader = glow::Shader::createFromSource(GL_VERTEX_SHADER,
                                                   R"(

                                                   in vec2 aPosition;
                                                   in vec3 aColor;

                                                   out vec3 vColor;

                                                   void main()
                                                   {
                                                    gl_Position = vec4(aPosition, 0, 1);
                                                    vColor = aColor;
                                                   }

                                                   )");

    mFragmentShader = glow::Shader::createFromSource(GL_FRAGMENT_SHADER,
                                                     R"(
                                                     in vec3 vColor;

                                                     out vec3 fColor;

                                                     void main()
                                                     {
                                                        fColor = vColor;
                                                     }
                                                     )");

    mProgram = glow::Program::create({mVertexShader, mFragmentShader});


    // create some initial vertices
    auto bb = tg::aabb2(-.2f, .2f);
    for (auto i = 0; i < 0; ++i)
    {
        auto p = uniform(rng, bb);
        auto v = tg::uniform<tg::dir2>(rng) * 0.6f;
        mVertices.push_back({p, v, {1, 0, 0}});
    }
}

void LiveApp::render(float elapsedSeconds)
{
    GLOW_SCOPED(clearColor, tg::color3::black);

    glViewport(0, 0, getWindowWidth(), getWindowHeight());
    glClear(GL_COLOR_BUFFER_BIT);

    // simulate particles

    for (auto i = 0; i < 10; ++i)
    {
        tg::vec2 v = {0, 1};
        v.y += uniform(rng, -.3f, .3f);
        v.x = uniform(rng, -.3f, .3f);
        mVertices.push_back({{0, 0}, v, {0, 1, 0}});
    }

    for (auto& v : mVertices)
    {
        v.lifetime += elapsedSeconds;

        v.pos += v.velocity * elapsedSeconds;

        v.velocity.y -= 0.4f * elapsedSeconds;

        v.velocity *= tg::pow(0.5f, elapsedSeconds / 4);

        if (v.velocity.x < 0 && v.pos.x < -1)
            v.velocity.x *= -1;
        if (v.velocity.x > 0 && v.pos.x > 1)
            v.velocity.x *= -1;

        if (v.velocity.y < 0 && v.pos.y < -1)
            v.velocity.y *= -1;
        // if (v.velocity.y > 0 && v.pos.y > 1)
        //     v.velocity.y *= -1;

        // v.color *= tg::pow(0.5f, elapsedSeconds / 4);
    }

    // delete dead particles
    for (auto i = int(mVertices.size()) - 1; i >= 0; --i)
    {
        if (false && mVertices[i].lifetime > 7)
        {
            std::swap(mVertices[i], mVertices.back());
            mVertices.pop_back();
        }
    }

    // upload it to the GPU
    mVertexData->bind().setData(mVertices);

    // actual drawing
    {
        auto prog = mProgram->use();

        glPointSize(4);
        mVertexArray->bind().draw();
    }
}

void LiveApp::onGui()
{
    // TODO
}
