#include "LiveApp.hh"

#include <imgui/imgui.h>

#include <GLFW/glfw3.h>

#include <typed-geometry/tg.hh>

#include <glow-extras/vector/backend/opengl.hh>
#include <glow-extras/vector/graphics2D.hh>
#include <glow-extras/vector/image2D.hh>
#include <glow/common/scoped_gl.hh>

namespace
{
bool collides_with(Projectile const& p, Asteroid const& a) { return distance(p.pos, a.pos) <= p.size + a.size; }
bool collides_with(Player const& p, Asteroid const& a) { return distance(p.pos, a.pos) <= a.size + 20; }
bool collides_with(Asteroid const& a, Asteroid const& b) { return distance(a.pos, b.pos) <= a.size + b.size; }
}

void LiveApp::shoot()
{
    Projectile p;
    p.pos = mPlayer.pos + mPlayer.dir * 40;
    p.velocity = mPlayer.dir * 500;
    mProjectiles.push_back(p);
    mProjectileCooldown = 0.05f;
}

void LiveApp::make_screen_shake(float amount) { mScreenshake = tg::max(mScreenshake, amount); }

void LiveApp::init()
{
    setGui(Gui::ImGui);

    // centering the player
    mPlayer.pos.x = 500;
    mPlayer.pos.y = 500;
    mPlayer.dir = tg::dir2::pos_x;

    auto const bb = tg::aabb2(-10000, 10000);
    for (auto i = 0; i < 20; ++i)
    {
        Asteroid a;
        a.pos = uniform(rng, bb);
        a.velocity = tg::uniform<tg::dir2>(rng) * uniform(rng, 0.0f, 10.0f);
        mAsteroids.push_back(a);
    }

    GlfwApp::init();
}

void LiveApp::update(float elapsedSeconds)
{
    auto const w = getWindowWidth();
    auto const h = getWindowHeight();

    auto const perform_motion_update = [&](auto& e) {
        e.pos += e.velocity * elapsedSeconds;

        while (e.pos.x > w)
            e.pos.x -= w;
        while (e.pos.y > h)
            e.pos.y -= h;
        while (e.pos.x < 0)
            e.pos.x += w;
        while (e.pos.y < 0)
            e.pos.y += h;
    };

    // update player
    {
        if (mKeyDownW)
            mPlayer.pos += mPlayer.dir * elapsedSeconds * 300;
        if (mKeyDownS)
            mPlayer.pos -= mPlayer.dir * elapsedSeconds * 300;
        if (mKeyDownA)
        {
            auto rot_angle = -360_deg * elapsedSeconds;
            auto [sa, ca] = tg::sin_cos(rot_angle);
            auto A = tg::mat2::from_rows({ca, -sa}, {sa, ca});
            mPlayer.dir = normalize(A * mPlayer.dir);
        }
        if (mKeyDownD)
        {
            auto rot_angle = 360_deg * elapsedSeconds;
            auto [sa, ca] = tg::sin_cos(rot_angle);
            auto A = tg::mat2::from_rows({ca, -sa}, {sa, ca});
            mPlayer.dir = normalize(A * mPlayer.dir);
        }

        perform_motion_update(mPlayer);
    }

    // screen shake
    mScreenshake *= tg::pow(0.5f, elapsedSeconds / 0.1f);

    // update projectile
    for (auto& p : mProjectiles)
    {
        perform_motion_update(p);

        // projectiles die after 2s
        p.time += elapsedSeconds;
        if (p.time > 2)
            p.is_dead = true;
    }

    // projectile collision
    std::vector<Asteroid> new_asteroids;
    for (auto& p : mProjectiles)
    {
        if (p.is_dead)
            continue;

        for (auto& a : mAsteroids)
            if (collides_with(p, a)) // collision detected!
            {
                p.is_dead = true; // delete the projectile

                a.size *= 0.7f; // downsize

                make_screen_shake(10);

                if (a.size < 10)
                {
                    a.is_dead = true;
                    continue;
                }

                Asteroid a2 = a; // copy

                auto new_dir = tg::uniform<tg::dir2>(rng);
                auto new_speed = uniform(rng, 100.f, 250.f);
                new_speed /= a.size / 20;
                a.velocity = new_dir * new_speed;
                a2.velocity = -new_dir * new_speed;

                a.pos += new_dir * a.size / 2;
                a2.pos -= new_dir * a.size / 2;

                new_asteroids.push_back(a2);
            }
    }
    mAsteroids.insert(mAsteroids.end(), new_asteroids.begin(), new_asteroids.end());

    // shooting
    mProjectileCooldown -= elapsedSeconds;
    if (mKeyDownSpace && mProjectileCooldown < 0)
        shoot();

    // update asteroids
    for (auto& a : mAsteroids)
    {
        perform_motion_update(a);
    }

    // asteroid player collision
    for (auto& a : mAsteroids)
        if (!a.is_dead && collides_with(mPlayer, a))
        {
            mPlayer.health -= 10;
            glow::info() << "HIT! lifes down to " << mPlayer.health;
            a.is_dead = true;
        }

    // asteroid collisions
    for (auto i = 0; i < int(mAsteroids.size()); ++i)
        for (auto j = i + 1; j < int(mAsteroids.size()); ++j)
        {
            auto& a = mAsteroids[i];
            auto& b = mAsteroids[j];
            if (collides_with(a, b) && !a.is_dead && !b.is_dead)
            {
                auto d = normalize(a.pos - b.pos);
                auto v_rel = a.velocity - b.velocity;

                if (dot(d, v_rel) < 0)
                {
                    a.velocity = tg::reflect(a.velocity, d);
                    b.velocity = tg::reflect(b.velocity, d);
                    a.velocity *= 0.6f;
                    b.velocity *= 0.6f;
                    make_screen_shake(2);
                }
            }
        }

    // game over
    if (mPlayer.health < 0)
        exit(0);

    // "cleanup"
    for (auto i = int(mProjectiles.size()) - 1; i >= 0; --i)
        if (mProjectiles[i].is_dead)
        {
            std::swap(mProjectiles[i], mProjectiles.back());
            mProjectiles.pop_back();
        }
    for (auto i = int(mAsteroids.size()) - 1; i >= 0; --i)
        if (mAsteroids[i].is_dead)
        {
            std::swap(mAsteroids[i], mAsteroids.back());
            mAsteroids.pop_back();
        }
}

void LiveApp::render(float elapsedSeconds)
{
    auto const w = getWindowWidth();
    auto const h = getWindowHeight();

    // clear screen
    GLOW_SCOPED(clearColor, tg::color3::black);
    glViewport(0, 0, w, h);
    glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    mImage.clear();
    auto g = graphics(mImage);

    auto shake_x = uniform(rng, -mScreenshake, mScreenshake);
    auto shake_y = uniform(rng, -mScreenshake, mScreenshake);

    // render scene
    for (auto ox : {-w, 0, w})
        for (auto oy : {-h, 0, h})
        {
            auto offset = tg::vec2(ox + shake_x, oy + shake_y);

            // render player
            auto [p0, p1, p2] = mPlayer.get_triangle();
            g.draw(tg::segment2(p0 + offset, p1 + offset), {tg::color3::green, 3});
            g.draw(tg::segment2(p1 + offset, p2 + offset), {tg::color3::green, 3});
            g.draw(tg::segment2(p2 + offset, p0 + offset), {tg::color3::green, 3});

            // render asteroids
            for (auto const& a : mAsteroids)
            {
                g.draw(tg::circle2(a.pos + offset, a.size), {tg::color3::white, 3});
            }

            // render projectiles
            for (auto const& p : mProjectiles)
            {
                g.draw(tg::circle2(p.pos + offset, p.size), {tg::color3::yellow, 3});
            }
        }
    // TODO

    // g.text({100, 100}, "Health: " + std::to_string(mPlayer.health), "sans", tg::color3::white);

    // render vector image
    mRenderer.render(mImage, w, h);
}

void LiveApp::onGui()
{
    // TODO
}

bool LiveApp::onKey(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_A && action == GLFW_PRESS)
        mKeyDownA = true;
    if (key == GLFW_KEY_A && action == GLFW_RELEASE)
        mKeyDownA = false;

    if (key == GLFW_KEY_W && action == GLFW_PRESS)
        mKeyDownW = true;
    if (key == GLFW_KEY_W && action == GLFW_RELEASE)
        mKeyDownW = false;

    if (key == GLFW_KEY_D && action == GLFW_PRESS)
        mKeyDownD = true;
    if (key == GLFW_KEY_D && action == GLFW_RELEASE)
        mKeyDownD = false;

    if (key == GLFW_KEY_S && action == GLFW_PRESS)
        mKeyDownS = true;
    if (key == GLFW_KEY_S && action == GLFW_RELEASE)
        mKeyDownS = false;

    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
        mKeyDownSpace = true;
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE)
        mKeyDownSpace = false;

    // TODO
    return false;
}
