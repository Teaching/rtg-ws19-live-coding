#pragma once

#include <glow-extras/glfw/GlfwApp.hh>

#include <glow-extras/vector/backend/opengl.hh>
#include <glow-extras/vector/image2D.hh>

#include <typed-geometry/tg-lean.hh>

struct Player
{
    tg::pos2 pos;
    tg::dir2 dir;
    tg::vec2 velocity;
    int health = 100;

    tg::triangle2 get_triangle() const
    {
        auto n = tg::dir2(-dir.y, dir.x);
        auto p0 = pos + dir * 40;
        auto p1 = pos - dir * 10 + n * 10;
        auto p2 = pos - dir * 10 - n * 10;
        return {p0, p1, p2};
    }
};

struct Asteroid
{
    tg::pos2 pos;
    tg::vec2 velocity;
    float size = 30;
    float health = 10;
    bool is_dead = false;

    tg::sphere2 get_sphere() const { return {pos, size}; }
};

struct Projectile
{
    tg::pos2 pos;
    tg::vec2 velocity;
    float size = 5;
    float time = 0;
    bool is_dead = false;
};

class LiveApp : public glow::glfw::GlfwApp
{
    tg::rng rng;
    glow::vector::OGLRenderer mRenderer;
    glow::vector::image2D mImage;

    Player mPlayer;
    std::vector<Asteroid> mAsteroids;
    std::vector<Projectile> mProjectiles;

    float mProjectileCooldown = 0;
    float mScreenshake = 0;

    void shoot();
    void make_screen_shake(float amount);

    bool mKeyDownW = false;
    bool mKeyDownA = false;
    bool mKeyDownS = false;
    bool mKeyDownD = false;
    bool mKeyDownSpace = false;

public:
    void init() override;

    void update(float elapsedSeconds) override;
    void render(float elapsedSeconds) override;

    void onGui() override;

    bool onKey(int key, int scancode, int action, int mods) override;
};
