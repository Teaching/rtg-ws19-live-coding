
in vec3 vNormal;
in vec3 vColor;

out vec3 fColor;

void main()
{
    fColor = vColor;
}
