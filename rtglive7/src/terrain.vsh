uniform sampler2D uHeightMap;

uniform int uTerrainSize;
uniform int uViewDistance;

uniform vec3 uCenterPos;

uniform mat4 uView;
uniform mat4 uProj;

in vec2 aPosition;

out vec3 vWorldPos;
out vec3 vNormal;
out vec3 vTangent;
out vec3 vBitangent;
out vec3 vColor;

vec3 worldPosAt(float x, float z)
{
    vec3 p;
    p.x = x;
    p.y = texture(uHeightMap, vec2(x, z) / uTerrainSize).x;
    p.y *= 3;
    p.z = z;
    return p;
}

void main()
{
    int ix = gl_InstanceID % uViewDistance;
    int iz = gl_InstanceID / uViewDistance;

    float wx = ix - uViewDistance / 2 + uCenterPos.x + aPosition.x;
    float wz = iz - uViewDistance / 2 + uCenterPos.z + aPosition.y;

    vWorldPos = worldPosAt(wx, wz);

    vec3 p_pos_x = worldPosAt(wx + 1, wz);
    vec3 p_neg_x = worldPosAt(wx - 1, wz);
    vec3 p_pos_z = worldPosAt(wx, wz + 1);
    vec3 p_neg_z = worldPosAt(wx, wz - 1);

    vNormal = -normalize(cross(p_pos_x - p_neg_x, p_pos_z - p_neg_z));
    vTangent = normalize(p_pos_x - p_neg_x);
    vBitangent = normalize(p_pos_z - p_neg_z);

    vColor = vec3(0,1,0);

    gl_Position = uProj * uView * vec4(vWorldPos, 1);
}
