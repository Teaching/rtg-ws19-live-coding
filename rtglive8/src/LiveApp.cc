#include "LiveApp.hh"

#include <imgui/imgui.h>

#include <chrono>

#include <GLFW/glfw3.h>

#include <typed-geometry/tg-std.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow/data/TextureData.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureRectangle.hh>

#include <glow-extras/geometry/Quad.hh>

float sampleTerrainAt(float x, float z, float terrainSize)
{
    auto const some_random_value = [](int x, int y) -> float {
        auto seed = (uint64_t(uint32_t(x)) << 32) | uint32_t(y);
        tg::rng rng;
        rng.seed(seed);
        return uniform(rng, -1.f, 1.f);
    };

    auto const value_noise = [&](float x, float y) {
        int ix = tg::ifloor(x);
        int iy = tg::ifloor(y);
        auto fx = x - ix;
        auto fy = y - iy;

        auto v00 = some_random_value(ix + 0, iy + 0);
        auto v01 = some_random_value(ix + 0, iy + 1);
        auto v10 = some_random_value(ix + 1, iy + 0);
        auto v11 = some_random_value(ix + 1, iy + 1);

        auto v0 = tg::mix(v00, v10, fx);
        auto v1 = tg::mix(v01, v11, fx);

        auto v = tg::mix(v0, v1, fy);

        return v;
    };

    auto const terrain_at = [&](float x, float y) {
        auto h = 0.0f;
        float scale = 0.033f;
        float amplitude = 1.f;
        for (auto i = 0; i < 7; ++i)
        {
            h += value_noise(x * scale, y * scale) * amplitude;
            scale *= 2;
            amplitude *= 0.7f;
        }
        return h;
    };

    return terrain_at(x, z)                     //
           + terrain_at(terrainSize - x - 1, z) //
           + terrain_at(x, terrainSize - z - 1) //
           + terrain_at(terrainSize - x - 1, terrainSize - z - 1);
}

void LiveApp::modifyTerrain(float elapsedSeconds)
{
    if (mDepthBelowCursor == 1)
        return;

    auto start = std::chrono::high_resolution_clock::now();

    // mapping world pos into terrain quad
    auto realPos = mWorldPosBelowCursor;
    while (realPos.x < 0)
        realPos.x += mTerrainSize;
    while (realPos.z < 0)
        realPos.z += mTerrainSize;
    while (realPos.x > mTerrainSize)
        realPos.x -= mTerrainSize;
    while (realPos.z > mTerrainSize)
        realPos.z -= mTerrainSize;
    realPos.y = 0;

    static tg::rng rng;

    for (auto y = 0; y < 128; ++y)
        for (auto x = 0; x < 128; ++x)
        {
            auto p = tg::pos3(x, 0, y);
            float d = distance(p, realPos);
            for (auto dx : {-1, 0, 1})
                for (auto dy : {-1, 0, 1})
                    d = tg::min(d, distance(p, realPos + tg::vec3(dx, 0, dy) * mTerrainSize));

            if (d > mModificationRadius)
                continue;

            auto& h = mHeightMap[y * mTerrainSize + x];

            switch (mEditMode)
            {
            case edit_mode::set_to_zero:
                h = 0;
                break;
            case edit_mode::add:
                h += tg::smoothstep(mModificationRadius, mModificationRadius * 0.5f, d) * elapsedSeconds;
                break;
            case edit_mode::reset:
            {
                float origH = sampleTerrainAt(x, y, mTerrainSize);
                h = tg::mix(origH, h, tg::pow(0.5f, elapsedSeconds));
            }
            break;
            case edit_mode::random_walk:
                h += uniform(rng, -1.f, 1.f) * elapsedSeconds * 10;
                break;
            }
        }

    // reupload data after modification
    auto tex = mHeightMapTexture->bind();
    tex.setData(GL_R32F, mTerrainSize, mTerrainSize, mHeightMap);
    tex.generateMipmaps();

    auto end = std::chrono::high_resolution_clock::now();
    glow::info() << "terrain modified in " << std::chrono::duration<double>(end - start).count() * 1000 << " ms";
}

void LiveApp::init()
{
    setGui(Gui::ImGui);

    GlfwApp::init();

    auto src_folder = glow::util::pathOf(__FILE__);

    mShaderMesh = glow::Program::createFromFile(src_folder + "/mesh");
    mShaderTerrain = glow::Program::createFromFile(src_folder + "/terrain");
    mShaderOutput = glow::Program::createFromFile(src_folder + "/output");

    mSkyBox = glow::TextureCubeMap::createFromData(glow::TextureData::createFromFileCube( //
        src_folder + "/../../textures/posx.jpg",                                          //
        src_folder + "/../../textures/negx.jpg",                                          //
        src_folder + "/../../textures/posy.jpg",                                          //
        src_folder + "/../../textures/negy.jpg",                                          //
        src_folder + "/../../textures/posz.jpg",                                          //
        src_folder + "/../../textures/negz.jpg",                                          //
        glow::ColorSpace::sRGB));

    mTexAlbedo = glow::Texture2D::createFromFile(src_folder + "/../../textures/debris_albedo.png", glow::ColorSpace::sRGB);
    mTexNormal = glow::Texture2D::createFromFile(src_folder + "/../../textures/debris_normal.png", glow::ColorSpace::Linear);
    mTexAO = glow::Texture2D::createFromFile(src_folder + "/../../textures/debris_ao.png", glow::ColorSpace::Linear);
    mTexHeight = glow::Texture2D::createFromFile(src_folder + "/../../textures/debris_height.png", glow::ColorSpace::Linear);
    mTexRoughness = glow::Texture2D::createFromFile(src_folder + "/../../textures/debris_roughness.png", glow::ColorSpace::Linear);

    mQuad = glow::geometry::Quad<>().generate();

    mTargetColor = glow::TextureRectangle::create(1, 1, GL_RGB8);
    mTargetDepth = glow::TextureRectangle::create(1, 1, GL_DEPTH_COMPONENT32);
    mFramebuffer = glow::Framebuffer::create("fColor", mTargetColor, mTargetDepth);

    // init terrain
    {
        tg::rng rng;
        mHeightMap.resize(mTerrainSize * mTerrainSize);
        for (auto z = 0; z < mTerrainSize; ++z)
            for (auto x = 0; x < mTerrainSize; ++x)
            {
                mHeightMap[idxOf(x, z)] = sampleTerrainAt(x, z, mTerrainSize);
            }

        mHeightMapTexture = glow::Texture2D::create();
        {
            auto tex = mHeightMapTexture->bind();
            tex.setData(GL_R32F, mTerrainSize, mTerrainSize, mHeightMap);
            tex.setWrap(GL_REPEAT, GL_REPEAT);
            tex.generateMipmaps();
        }

        if (false) // old terrain
            for (auto z = 0; z < mTerrainSize - 1; ++z)
                for (auto x = 0; x < mTerrainSize - 1; ++x)
                {
                    auto make_pos = [&](int x, int z) {
                        auto h = mHeightMap[idxOf(x, z)];
                        return tg::pos3(x - mTerrainSize / 2, h, z - mTerrainSize / 2);
                    };

                    auto p00 = make_pos(x + 0, z + 0);
                    auto p01 = make_pos(x + 0, z + 1);
                    auto p10 = make_pos(x + 1, z + 0);
                    auto p11 = make_pos(x + 1, z + 1);

                    debugMesh.add_triangle(p00, p11, p10, tg::color3::green);
                    debugMesh.add_triangle(p00, p01, p11, tg::color3::green);
                }
    }

    auto c = tg::pos3(0, 1, 0);
    auto dx = tg::vec3::unit_x;
    auto dy = tg::vec3::unit_y;
    auto dz = tg::vec3::unit_z;
    debugMesh.add_triangle(c - dz * 0.1f, c + dz * 0.1f, c + dx, tg::color3::red);
    debugMesh.add_triangle(c - dx * 0.1f, c + dx * 0.1f, c + dz, tg::color3::blue);
    debugMesh.add_triangle(c - dz * 0.1f, c + dz * 0.1f, c + dy, tg::color3::green);
    debugMesh.add_triangle(c - dx * 0.1f, c + dx * 0.1f, c + dy, tg::color3::green);
    debugMesh.upload();
}

void LiveApp::update(float elapsedSeconds)
{
    // TODO

    if (mLeftDown)
        modifyTerrain(elapsedSeconds);

    auto const speed = 50.f;

    if (mKeyDownW)
        cam.pos += cam.dir * elapsedSeconds * speed;
    if (mKeyDownS)
        cam.pos -= cam.dir * elapsedSeconds * speed;
    if (mKeyDownD)
        cam.pos += cam.right() * elapsedSeconds * speed;
    if (mKeyDownA)
        cam.pos -= cam.right() * elapsedSeconds * speed;
}

void LiveApp::render(float elapsedSeconds)
{
    auto const w = getWindowWidth();
    auto const h = getWindowHeight();

    cam.aspect_ratio = w / float(h);

    // camera matrices
    auto proj = cam.proj();
    auto view = cam.view();
    auto inv_proj = inverse(proj);
    auto inv_view = inverse(view);

    // render scene
    {
        auto fb = mFramebuffer->bind();

        // clear screen
        GLOW_SCOPED(clearColor, tg::color3::black);
        GLOW_SCOPED(enable, GL_DEPTH_TEST);
        GLOW_SCOPED(enable, GL_CULL_FACE);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        GLOW_SCOPED(polygonMode, mWireframe ? GL_LINE : GL_FILL);

        {
            auto shader = mShaderMesh->use();
            shader["uProj"] = proj;
            shader["uView"] = view;

            GLOW_SCOPED(disable, GL_CULL_FACE);
            debugMesh.vertexArray->bind().draw();
        }

        // render terrain
        {
            auto shader = mShaderTerrain->use();
            shader["uProj"] = proj;
            shader["uView"] = view;
            shader["uTerrainSize"] = mTerrainSize;
            shader["uViewDistance"] = mViewDistance;
            shader["uHeightMap"] = mHeightMapTexture;
            shader["uCenterPos"] = tg::round(cam.pos);
            shader["uCamPos"] = cam.pos;

            shader["uTexAlbedo"] = mTexAlbedo;
            shader["uTexNormal"] = mTexNormal;
            shader["uTexAO"] = mTexAO;
            shader["uTexHeight"] = mTexHeight;
            shader["uTexRoughness"] = mTexRoughness;

            shader["uSkyBox"] = mSkyBox;

            shader["uEpsMultiplier"] = mEpsMultiplier;

            shader["uRuntime"] = (float)glfwGetTime();

            shader["uCursorPos"] = mWorldPosBelowCursor;
            shader["uModificationRadius"] = mModificationRadius;

            mQuad->bind().draw(mViewDistance * mViewDistance);
        }
    }

    // read back screen pixel
    {
        auto fb = mFramebuffer->bind();

        // GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, void *pixels

        auto mx = int(getMousePosition().x);
        auto my = int(getWindowHeight() - getMousePosition().y - 1);

        if (0 <= mx && 0 <= my && mx < getWindowWidth() && my < getWindowHeight())
        {
            glReadPixels(mx, my, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &mDepthBelowCursor);

            tg::pos3 ndc = {mx / (getWindowWidth() - 1.f) * 2 - 1,  //
                            my / (getWindowHeight() - 1.f) * 2 - 1, //
                            mDepthBelowCursor * 2 - 1};

            mWorldPosBelowCursor = inv_view * inv_proj * ndc;
        }
    }

    // write output
    {
        auto shader = mShaderOutput->use();
        shader["uTargetColor"] = mTargetColor;
        shader["uTargetDepth"] = mTargetDepth;
        shader["uSkyBox"] = mSkyBox;
        shader["uInvProj"] = inv_proj;
        shader["uInvView"] = inv_view;

        GLOW_SCOPED(disable, GL_DEPTH_TEST);
        GLOW_SCOPED(disable, GL_CULL_FACE);

        mQuad->bind().draw();
    }
}

void LiveApp::onResize(int w, int h)
{
    GlfwApp::onResize(w, h);

    mTargetColor->bind().resize(w, h);
    mTargetDepth->bind().resize(w, h);
}

void LiveApp::onGui()
{
    if (ImGui::Begin("debug"))
    {
        ImGui::Checkbox("Wireframe", &mWireframe);

        auto mp = getMousePosition();
        ImGui::Text("mouse x: %f", mp.x);
        ImGui::Text("mouse y: %f", mp.y);
        ImGui::Text("depth: %f", mDepthBelowCursor);
        ImGui::Text("world x: %f", mWorldPosBelowCursor.x);
        ImGui::Text("world y: %f", mWorldPosBelowCursor.y);
        ImGui::Text("world z: %f", mWorldPosBelowCursor.z);
        ImGui::SliderInt("view distance", &mViewDistance, 10, 1000);

        ImGui::SliderFloat("eps mult", &mEpsMultiplier, .1f, 10);

        ImGui::SliderFloat("edit radius", &mModificationRadius, 0.f, 10.f);

        if (ImGui::RadioButton("edit: set to zero", mEditMode == edit_mode::set_to_zero))
            mEditMode = edit_mode::set_to_zero;
        if (ImGui::RadioButton("edit: add", mEditMode == edit_mode::add))
            mEditMode = edit_mode::add;
        if (ImGui::RadioButton("edit: reset", mEditMode == edit_mode::reset))
            mEditMode = edit_mode::reset;
        if (ImGui::RadioButton("edit: random walk", mEditMode == edit_mode::random_walk))
            mEditMode = edit_mode::random_walk;
    }
    ImGui::End();
}

bool LiveApp::onMouseButton(double x, double y, int button, int action, int mods, int clickCount)
{
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
        setCursorMode(glow::glfw::CursorMode::Disabled);

    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
        setCursorMode(glow::glfw::CursorMode::Normal);

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
        mLeftDown = true;
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
        mLeftDown = false;

    return true;
}

bool LiveApp::onMousePosition(double x, double y)
{
    static double lastX = x;
    static double lastY = y;

    auto dx = float(x - lastX);
    auto dy = float(y - lastY);

    auto cam_speed = -360_deg / 2000;

    if (isMouseButtonDown(1))
    {
        auto rotX = tg::rotation_around(cam_speed * dx, cam.up);
        cam.dir = normalize(rotX * cam.dir);
        auto rotY = tg::rotation_around(cam_speed * dy, cam.right());
        cam.dir = normalize(rotY * cam.dir);
    }

    lastX = x;
    lastY = y;

    return true;
}

bool LiveApp::onMouseScroll(double sx, double sy)
{
    mModificationRadius += float(sy) * 0.5f;
    mModificationRadius = tg::clamp(mModificationRadius, 1.0f, 50.f);

    return false;
}

bool LiveApp::onKey(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_A && action == GLFW_PRESS)
        mKeyDownA = true;
    if (key == GLFW_KEY_A && action == GLFW_RELEASE)
        mKeyDownA = false;

    if (key == GLFW_KEY_W && action == GLFW_PRESS)
        mKeyDownW = true;
    if (key == GLFW_KEY_W && action == GLFW_RELEASE)
        mKeyDownW = false;

    if (key == GLFW_KEY_D && action == GLFW_PRESS)
        mKeyDownD = true;
    if (key == GLFW_KEY_D && action == GLFW_RELEASE)
        mKeyDownD = false;

    if (key == GLFW_KEY_S && action == GLFW_PRESS)
        mKeyDownS = true;
    if (key == GLFW_KEY_S && action == GLFW_RELEASE)
        mKeyDownS = false;

    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
        mKeyDownSpace = true;
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE)
        mKeyDownSpace = false;

    return false;
}

void mesh::upload()
{
    arrayBuffer = glow::ArrayBuffer::create();
    arrayBuffer->defineAttribute(&vertex::pos, "aPosition");
    arrayBuffer->defineAttribute(&vertex::normal, "aNormal");
    arrayBuffer->defineAttribute(&vertex::color, "aColor");
    arrayBuffer->bind().setData(vertices);

    vertexArray = glow::VertexArray::create(arrayBuffer, GL_TRIANGLES);
}
