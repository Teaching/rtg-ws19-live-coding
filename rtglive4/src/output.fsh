uniform sampler2DRect uTargetColor;

out vec3 fColor;

void main()
{
    fColor = texture(uTargetColor, gl_FragCoord.xy).rgb;
}
