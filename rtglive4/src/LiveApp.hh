#pragma once

#include <glow-extras/glfw/GlfwApp.hh>

#include <typed-geometry/tg-lean.hh>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/VertexArray.hh>

#include <vector>

struct vertex
{
    tg::pos3 pos;
    tg::vec3 normal;
    tg::color3 color;
};

struct mesh
{
    std::vector<vertex> vertices;

    glow::SharedArrayBuffer arrayBuffer; // vertex data
    glow::SharedVertexArray vertexArray; // mesh config

    void add_triangle(tg::pos3 v0, tg::pos3 v1, tg::pos3 v2, tg::color3 c)
    {
        auto n = normalize(cross(v1 - v0, v2 - v0));
        vertices.push_back({v0, n, c});
        vertices.push_back({v1, n, c});
        vertices.push_back({v2, n, c});
    }

    void upload();
};

struct camera
{
    float aspect_ratio = 1.f;
    tg::pos3 pos = {8, 3, 8};
    tg::dir3 dir = normalize(tg::pos3::zero - pos);
    tg::dir3 up = {0, 1, 0};

    tg::dir3 right() const { return normalize(cross(dir, up)); }

    tg::mat4 proj() const { return tg::perspective_opengl(70_deg, aspect_ratio, 0.1f, 1000.f); }
    tg::mat4 view() const { return tg::look_at_opengl(pos, dir, tg::vec3::unit_y); }
};

class LiveApp : public glow::glfw::GlfwApp
{
    mesh debugMesh;
    camera cam;

    glow::SharedProgram mShaderMesh;
    glow::SharedProgram mShaderTerrain;

    glow::SharedTexture2D mHeightMapTexture;

    glow::SharedProgram mShaderOutput;
    glow::SharedVertexArray mQuad;

    glow::SharedFramebuffer mFramebuffer;
    glow::SharedTextureRectangle mTargetColor;
    glow::SharedTextureRectangle mTargetDepth;

    int mTerrainSize = 128;
    std::vector<float> mHeightMap;

    int mViewDistance = 200;

    int idxOf(int x, int z) const { return z * mTerrainSize + x; }

    bool mKeyDownW = false;
    bool mKeyDownA = false;
    bool mKeyDownS = false;
    bool mKeyDownD = false;
    bool mKeyDownSpace = false;

    bool mWireframe = false;

    float mDepthBelowCursor;
    tg::pos3 mWorldPosBelowCursor;

public:
    void init() override;

    void update(float elapsedSeconds) override;
    void render(float elapsedSeconds) override;
    void onResize(int w, int h) override;

    void onGui() override;

    bool onMouseButton(double x, double y, int button, int action, int mods, int clickCount) override;
    bool onMousePosition(double x, double y) override;
    bool onKey(int key, int scancode, int action, int mods) override;
};
