# ===============================================
# Configure executable

file(GLOB_RECURSE SOURCES
    "src/*.cc"
    "src/*.hh"
    "src/*.*sh"
    "src/*.glsl"
)

source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${SOURCES})

add_executable(rtglive4 ${SOURCES})

target_link_libraries(rtglive4 PUBLIC
    glow
    glow-extras
    typed-geometry
    ctracer
    imgui
)

target_include_directories(rtglive4 PUBLIC "src")


# ===============================================
# Compile flags

if (MSVC)
    target_compile_options(rtglive4 PUBLIC
        /MP
    )
else()
    target_compile_options(rtglive4 PUBLIC
        -Wall
        -Wno-unused-variable
    )
    target_link_libraries(rtglive4 PUBLIC
        -lstdc++fs
    )
endif()
