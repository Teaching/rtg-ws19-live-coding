uniform sampler2D uHeightMap;
uniform sampler2D uGrassMap;

uniform int uTerrainSize;
uniform int uViewDistance;

uniform vec3 uCenterPos;

uniform mat4 uView;
uniform mat4 uProj;

uniform float uRuntime;

in vec2 aGrassPos;
in vec2 aSeedPos;

out vec3 vBaseColor;
out vec3 vWorldPos;
out vec2 vGrassPos;

vec2 windAt(float x, float z)
{
    return vec2(0.5,0.5) * (cos(x / 6.2 + z / 10.123 + uRuntime) + 0.3 * cos(x / -2.3 + z / 4.3 + uRuntime * 2));
}

vec3 worldPosAt(float x, float z)
{
    vec3 p;
    p.x = x;
    p.y = texture(uHeightMap, vec2(x, z) / uTerrainSize).x;
    p.y *= 3;
    p.z = z;
    return p;
}

void main()
{
    vGrassPos = aGrassPos;

    vec3 pos;
    pos = worldPosAt(aSeedPos.x, aSeedPos.y);

    vec3 grass_data = texture(uGrassMap, aSeedPos / uTerrainSize).xyz;

    float grass_alpha = 0.5 + 0.5 * cos(312.2 * gl_InstanceID);

    if (grass_alpha >= grass_data.x)
    {
        gl_Position = vec4(0,0,0,0);
        return;
    }

    float size = smoothstep(grass_data.x, max(0.f, grass_data.x - .2f), grass_alpha);

    vBaseColor = vec3(0);
    vBaseColor = mix(vec3(1,0,0), vec3(0,1,0), grass_data.z);
    vBaseColor.g += 0.4f * cos(842.13 * gl_InstanceID);
    vBaseColor.r += 0.4f * cos(-642.13 * gl_InstanceID);
    vBaseColor = clamp(vBaseColor, vec3(0), vec3(1));

    float a = 123.134 * gl_InstanceID;
    float sa = sin(a);
    float ca = cos(a);

    float w = 0.2f;
    float l = 1.5f;
    w += 0.05f * cos(7123.2 * gl_InstanceID);
    l += 0.25f * cos(1723.2 * gl_InstanceID);

    float c = 0.8f;
    c += 0.4 * cos(523.2 * gl_InstanceID);

    vec2 right = vec2(sa, ca);
    vec2 front = vec2(-ca, sa);

    pos.y += size * aGrassPos.y * l * grass_data.y;
    pos.xz += size * right * (aGrassPos.x - 0.5) * w;

    pos.xz += size * c * front * aGrassPos.y * aGrassPos.y;
    pos.xz += size * windAt(aSeedPos.x, aSeedPos.y) * aGrassPos.y;


    vWorldPos = pos;

    gl_Position = uProj * uView * vec4(vWorldPos, 1);
}
