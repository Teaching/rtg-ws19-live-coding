
in vec2 vGrassPos;
in vec3 vBaseColor;

out vec3 fColor;

void main()
{
    fColor = vBaseColor * vGrassPos.y * vGrassPos.y;
}
