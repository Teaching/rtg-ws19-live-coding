uniform sampler2D uTexAlbedo;
uniform sampler2D uTexNormal;
uniform sampler2D uTexAO;
uniform sampler2D uTexHeight;
uniform sampler2D uTexRoughness;

uniform samplerCube uSkyBox;

uniform float uEpsMultiplier;
uniform float uRuntime;

uniform vec3 uCamPos;

uniform vec3 uCursorPos;
uniform float uModificationRadius;

in vec3 vNormal;
in vec3 vTangent;
in vec3 vBitangent;
in vec3 vColor;
in vec3 vWorldPos;

out vec3 fColor;

void main()
{
    vec2 uv = vWorldPos.xz / 5;

    float apparentHeight = 0.1;

    vec3 N = normalize(vNormal); // vec3(0,1,0);
    vec3 T = normalize(vTangent); // vec3(1,0,0);
    vec3 B = normalize(vBitangent); // vec3(0,0,-1);

    // vec3 lightPos = vec3(4 * cos(uRuntime), 3 + 2 * cos(uRuntime * 0.3), 4 * sin(uRuntime));
    // vec3 L = normalize(lightPos - vWorldPos);
    vec3 L = vec3(0,1,0);
    vec3 V = normalize(uCamPos - vWorldPos);

    vec3 albedo = texture(uTexAlbedo, uv).rgb;
    float ao = texture(uTexAO, uv).x;
    float h = texture(uTexHeight, uv).x;

    vec3 normalMap = texture(uTexNormal, uv).rgb;
    normalMap.xy = normalMap.xy * 2 - 1;

    // fColor = N;
    N = normalize(
                T * normalMap.x +
                B * normalMap.y +
                N * normalMap.z
                // + N * 215
            );

    vec3 R = reflect(-V, N);

    // fColor = N;
    fColor = albedo * ao * max(0, dot(N, L));
    // fColor = textureLod(uSkyBox, R, 5).rgb;
    fColor += 0.1 * texture(uSkyBox, R).rgb;
    // fColor = albedo;

    fColor *= vColor;

    float d = distance(uCursorPos.xz, vWorldPos.xz);

    if (d < 0.1)
        fColor = vec3(1,0,0);

    if (d < uModificationRadius)
    {
        float a = smoothstep(uModificationRadius - .5, uModificationRadius, d);

        fColor = mix(fColor, vec3(0,0,1), a);
    }

    // fColor = vColor * max(0, normalize(vNormal).y);
}
